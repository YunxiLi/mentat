########################################################
# Function: BuildDb
# Description: convert csv files into a database
#
# Input: 
# dbName -> name of database file
# csvFolder -> the folder that holds the csv files to be processed
# 
# Output:
# A SQL database that is placed into the same folder as the caller
######################################################### 

def BuildDb(dbName, csvFolder):
    import os, sqlite3
    import pandas as pd
    
    # delete existing database, if any
    try:
        os.remove(dbName)
    except OSError:
        pass
    
    con = sqlite3.connect(dbName)
    csvTbl = pd.DataFrame()
    csvList = [file for file in os.listdir(csvFolder) if file.endswith(".csv")] # list of all csv files
    cnt = 0
    
    # convert files into database
    for csvFile in csvList:
        cnt += 1
        print "Processing %d of %d" %(cnt, len(csvList))
        csvFilePath = os.path.join(csvFolder, csvFile)        
        csvTbl = pd.read_csv(csvFilePath, encoding = 'ISO-8859-1')
        csvTbl.to_sql('Bike', con, if_exists='append')
    
    con.close()